#!/usr/bin/python

# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import sys
import argparse
from os import path as osp

if __name__ == "__main__":
    if osp.isfile(
        osp.join(
            osp.dirname(osp.dirname(osp.realpath(__file__))), ".portmod_not_installed"
        )
    ):
        sys.path.insert(0, osp.dirname(osp.dirname(osp.realpath(__file__))))

    parser = argparse.ArgumentParser(
        description="Quality assurance program for the mod repository"
    )
    parser.add_argument(
        "mode",
        metavar="[mode]",
        nargs="?",
        choices=["manifest", "manifest-check", "scan"],
        help='Mode in which to run. One of "manifest", "manifest-check", "scan". Default is "scan"',
    )

    args = parser.parse_args()

    import glob
    import yaml
    from portmod.main import pybuild_validate, pybuild_manifest
    from portmod.repo.metadata import (
        get_categories,
        get_repo_root,
        license_exists,
        Person,
        Group,
    )
    from portmod.log import err, ERROR
    from portmod.repo.list import read_list

    repo_root = get_repo_root(os.getcwd())

    if repo_root is None:
        err(
            "Cannot find repository for the current directory. Please run from within the repository you wish to inspect"
        )

    if args.mode is None or args.mode == "scan":
        # Run pybuild validate on every pybuild in repo
        for category in get_categories(repo_root):
            for file in glob.glob(os.path.join(repo_root, category, "*", "*.pybuild")):
                pybuild_validate(file)

        ## Check files in metadata. These may not exist, as they might be inherited from another repo instead
        # Check metadata/arch.list
        path = os.path.join(repo_root, "metadata", "arch.list")
        if os.path.exists(path):
            lines = read_list(path)
            for arch in lines:
                if " " in category:
                    err(
                        'arch.list: in entry "{}". Architectures cannot contain spaces'.format(
                            arch
                        )
                    )

        # Check metadata/categories.list
        path = os.path.join(repo_root, "metadata", "categories.list")
        if os.path.exists(path):
            lines = read_list(path)
            for category in lines:
                if " " in category:
                    err(
                        'categories.list: in category "{}". Categories cannot contain spaces'.format(
                            category
                        )
                    )

        # Check metadata/groups.yaml
        path = os.path.join(repo_root, "metadata", "groups.yaml")
        if os.path.exists(path):
            with open(path, mode="r") as file:
                groups = yaml.load(file)
                for name, group in groups.items():
                    if not "desc" in group:
                        err(
                            'groups.yaml: Group "{}" is missing "desc" field'.format(
                                name
                            )
                        )
                    if not "members" in group:
                        err(
                            'Group "{}" in "{}" is missing "desc" field'.format(
                                name, path
                            )
                        )
                    elif group.get("members") is not None:
                        for member in group.get("members"):
                            if type(member) is not Person and type(member) is not Group:
                                err(
                                    'groups.yaml: Invalid entry "{}" in members of group "{}"'.format(
                                        member, group
                                    )
                                )

        # Check metadata/license_groups.yaml
        # All licenses should exist in licenses/LICENSE_NAME
        path = os.path.join(repo_root, "metadata", "license_groups.yaml")
        if os.path.exists(path):
            with open(path, mode="r") as file:
                groups = yaml.load(file)
                for key, value in groups.items():
                    if value is not None:
                        for license in value.split():
                            if not license_exists(repo_root, license):
                                err(
                                    'license_groups.yaml: License "{}" in group {} does not exist in licenses directory'.format(
                                        license, key
                                    )
                                )

        # Check metadata/repo_name
        path = os.path.join(repo_root, "metadata", "repo_name")
        if os.path.exists(path):
            lines = read_list(path)
            if len(lines) == 0:
                err("repo_name: repo name cannot be empty".format(path))
            elif len(lines) > 1:
                err(
                    "repo_name: Extra lines detected. File must contain just the repo name."
                )
            elif " " in lines[0]:
                err("repo_name: Repo name must not contain spaces.")

        # Check metadata/use.yaml
    elif args.mode == "manifest":
        # Run pybuild manifest on every pybuild in repo
        for category in get_categories(repo_root):
            for file in glob.glob(os.path.join(repo_root, category, "*", "*.pybuild")):
                pybuild_manifest(file)
    elif args.mode == "manifest-check":
        # TODO: check that files in repo also occur in the manifest
        # Check that distfiles occur in the manifest
        pass

    if ERROR:
        err("Operation completed with errors.")
        exit(1)
