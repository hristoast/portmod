# Portmod
A cli tool to manage mods for OpenMW

## Python Dependencies
Can be installed through pip, or via your package manager.

- patool: http://wummel.github.io/patool/
- colorama
- appdirs
- black
- GitPython
- PyYAML
- progressbar2

## Other Dependencies
### omwcmd
A native cli tool written in Rust that makes use of esplugin to quickly parse elder scrolls plugin files. Used for getting the masters of plugins.

https://gitlab.com/bmwinger/omwcmd

## Installing on Linux

To install into `/usr/bin` (requires root), run
```sudo ./setup.py install```

To install into `~/.local/bin`, run
```./setup.py install --user```

You will want to create a config file in ~/.config/portmod/portmod.cfg. A sample is included in the root of the repository. Primarily you will want to set the USE variable and the ARCH variable to reflect your openmw setup.

## Installing on Windows

Note that Windows support is currently untested, however there is no reason to suggest that it will not work on Windows.

In addition to the above dependencies, you will need to install Git, if it has not already been installed. The python dependencies can be installed with pip, which comes with Python.

https://git-scm.com/download/win

## Basic Usage

Before installing other mods, you should explicitly install the morrowind mod. This pybuild automatically detects the location of your morrowind installation and "installs it" by creating a softlink to this installation. This is done so that the morrowind data files can be treated just like other data files. You may also want to install omwllf to merge levelled lists.

Mods can be installed by passing the relevant atoms as command line arguments. E.g.:
`mwmerge morrowind omwllf`

You can search for mods using the `--search`/`-s` (searches name only) and `--searchdesc`/`-S` (searches name and description) options.

Specific versions of mods can be installed by including the version number: `mwmerge abandoned-flat-2.0`

Specified mods will be automatically be downloaded, configured and installed.

The `-c` flag will remove the specified mods and all mods that depend on, or are dependencies of, the specified mods.

The `-C` flag will remove the specified mods, ignoring dependencies

## Notes

- Make sure the openmw-launcher is not running. If you add mods when the openmw-launcher is running the mods will be removed from the config file when the openmw-launcher is closed.
- When installing mods, relevant plugins are enabled automatically. There is no need to enable plugins using openmw-launcher.
- In general, manual configuration is not recommended when using Portmod as it may lead to undesired behaviour, notably, the potential loss of your changes when running updates. If there is a configuration step that a pybuild omits you have the following options:
    1. Report the issue and wait for a fix
    2. Fix the pybuild yourself:
        - Create a local repository and add it to repos.cfg in the config directory
        - Copy and modify the offending pybuild.
        - Once you've added and tested the fix, please submit the changes to the repository so that others can benefit from the change.

This project is not affiliated with OpenMW
