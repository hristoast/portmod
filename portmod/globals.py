# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import sys
import tempfile
import configparser
import git
from appdirs import user_data_dir, user_cache_dir, user_config_dir

APP_NAME = "portmod"
APP_AUTHOR = "portmod"

HOME = os.path.expanduser("~")
OPENMW_CONFIG_DIR = user_config_dir("openmw", "openmw")

# Manually override OPENMW_CONFIG_DIR paths on win32 and darwin
#   according to https://openmw.readthedocs.io/en/stable/reference/modding/paths.html
if sys.platform == "darwin":
    OPENMW_CONFIG_DIR = os.path.join(HOME, "/Library/Preferences/openmw")
elif sys.platform == "win32":
    OPENMW_CONFIG_DIR = os.path.join(HOME, "Documents", "my games", "openmw")

OPENMW_CONFIG = os.path.join(OPENMW_CONFIG_DIR, "openmw.cfg")

TMP_DIR = os.path.join(tempfile.gettempdir(), "portmod")

PORTMOD_CONFIG_DIR = user_config_dir(APP_NAME, APP_AUTHOR)
SET_DIR = os.path.join(PORTMOD_CONFIG_DIR, "sets")
PORTMOD_CONFIG = os.path.join(PORTMOD_CONFIG_DIR, "portmod.cfg")

PORTMOD_LOCAL_DIR = user_data_dir(APP_NAME, APP_AUTHOR)
MOD_DIR = os.path.join(PORTMOD_LOCAL_DIR, "mods")
CACHE_DIR = user_cache_dir(APP_NAME, APP_AUTHOR)

INSTALLED_DB = os.path.join(PORTMOD_LOCAL_DIR, "db")
# Ensure that INSTALLED_DB exists
if not os.path.exists(INSTALLED_DB):
    # Initialize as git repo
    os.makedirs(INSTALLED_DB)
    gitrepo = git.Repo.init(INSTALLED_DB)

if os.path.exists(PORTMOD_CONFIG):
    mmm_config = configparser.ConfigParser()
    mmm_config.read(PORTMOD_CONFIG)
    if mmm_config["general"]:
        GLOBAL_USE = mmm_config["general"].get("USE", "").split()
        ARCH = mmm_config["general"].get("ARCH", None)
        OPENMW_CONFIG = os.path.expanduser(
            mmm_config["general"].get("OPENMW_CONFIG", OPENMW_CONFIG)
        )
        ACCEPT_KEYWORDS = mmm_config["general"].get("ACCEPT_KEYWORDS", ARCH).split()
        ACCEPT_LICENSE = mmm_config["general"].get("ACCEPT_LICENSE", "* -@EULA").split()
    else:
        GLOBAL_USE = []
        ARCH = "openmw"
        ACCEPT_KEYWORDS = [ARCH]
        ACCEPT_LICENSE = ["*", "-@EULA"]
else:
    print("Could not find portmod.cfg. Creating new config: {}".format(PORTMOD_CONFIG))
    os.makedirs(PORTMOD_CONFIG_DIR)
    with open(PORTMOD_CONFIG, mode="w") as config:
        print("[general]", file=config)
        print("USE=", file=config)
        print("ARCH=openmw", file=config)
    GLOBAL_USE = []
    ARCH = "openmw"
    ACCEPT_KEYWORDS = [ARCH]
