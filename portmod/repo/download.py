# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import urllib.request
import os
from portmod.repo.util import get_hash
from portmod.globals import CACHE_DIR


def get_filename(basename):
    return os.path.join(CACHE_DIR, "downloads", basename)


def download(url, destName):
    os.makedirs(os.path.join(CACHE_DIR, "downloads"), exist_ok=True)
    urllib.request.urlretrieve(url, get_filename(destName))


def check_hash(filename, checksum):
    return get_hash(filename) == checksum


def get_download(name, checksum):
    if os.path.exists(get_filename(name)) and check_hash(get_filename(name), checksum):
        return get_filename(name)
    return False


def download_mod(mod):
    download_list = []
    for source in mod.SOURCES:
        cached = get_download(source.NAME, source.SHASUM)
        if cached:
            # Download is in cache. Nothing to do.
            print("Using " + cached)
            download_list.append(cached)
        else:
            # Download archive
            filename = get_filename(source.NAME)
            print("Downloading...")
            download(source.URL, source.NAME)
            print("Downloaded " + filename)
            if not check_hash(filename, source.SHASUM):
                print("Error: Source file {} has invalid checksum!".format(source.NAME))
                return False

            # Finally, add to list
            download_list.append(filename)

    return download_list
