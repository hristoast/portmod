# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import hashlib
from portmod.prompt import prompt_bool
from portmod.repo.keywords import accepts, accepts_testing, get_keywords, add_keyword
from portmod.repo.metadata import get_license_groups, has_eula, is_license_accepted
from portmod.globals import ARCH

BUF_SIZE = 65536


class KeywordDep:
    def __init__(self, atom, keyword):
        self.atom = atom
        self.keyword = keyword


class LicenseDep:
    def __init__(self, atom, license, is_eula):
        self.atom = atom
        self.license = license
        self.is_eula = is_eula


def select_mod(modlist):
    # Since mod Licenses rarely change between versions, choose a mod version based on keywords
    #   and accept it if the license is accepted
    (mod, usedep) = select_mod_by_use(modlist)

    if not is_license_accepted(mod):
        return (mod, LicenseDep(mod.CMN, mod.LICENSE, has_eula(mod)))
    elif usedep is not None:
        return (mod, KeywordDep("=" + mod.CM, usedep))
    else:
        return (mod, None)


def select_mod_by_use(modlist):
    if len(modlist) == 0:
        return None

    filtered = list(
        filter(lambda mod: accepts(get_keywords(mod.ATOM), mod.KEYWORDS), modlist)
    )

    if len(filtered) == 0:
        # No mods were accepted. Prompt user to add an exception for the best version in this modlist
        unstable = list(
            filter(lambda mod: accepts_testing(ARCH, mod.KEYWORDS), modlist)
        )
        if len(unstable) > 0:
            newest_unstable = get_newest_mod(unstable)
            return (newest_unstable, "~" + ARCH)

        # There was no mod that would be accepted by enabling testing. Try enabling unstable
        newest = get_newest_mod(modlist)
        return (newest, "**")

    return (get_newest_mod(filtered), None)


def get_newest_mod(modlist):
    newest = None
    for mod in modlist:
        if not newest:
            newest = mod
            newest_ver = mod.MV.split(".")
        else:
            ver = mod.MV.split(".")
            for index, val in enumerate(ver):
                if val > newest_ver[index]:
                    newest = mod
                    newest_ver = ver
                    break
                elif val < newest_ver[index]:
                    break
    return newest


def get_hash(filename):
    sha = hashlib.sha512()
    with open(filename, mode="rb") as archive:
        while True:
            data = archive.read(BUF_SIZE)
            if not data:
                break
            sha.update(data)
    return sha.hexdigest()
