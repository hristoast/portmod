# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import importlib.machinery
import glob
import configparser
import os
from portmod.log import warn
from portmod.repo.atom import parse_atom, atom_sat
from portmod.globals import PORTMOD_CONFIG, INSTALLED_DB
from portmod.repos import REPOS
from portmod.repo.metadata import get_categories
from portmod.repo.use import get_use

# We store a cache of mods so that they are only loaded once when doing dependency resolution.
# Stores key-value pairs of the form (filename, Mod Object)
__mods = {}


def load_file(file):
    filename, _ = os.path.splitext(os.path.basename(file))
    loader = importlib.machinery.SourceFileLoader(filename, file)
    try:
        mod = loader.load_module().Mod()
    except Exception as e:
        warn(e)
        warn('Could not load pybuild "{}"'.format(filename))
        return None
    mod.FILE = os.path.abspath(file)
    mod.INSTALLED = False
    (enabled, disabled) = get_use(mod)
    mod.USE = enabled
    return mod


def load_all_installed():
    mods = (
        []
    )  # We will return every single mod matching this name. There may be multiple versions in different repos, as well versions with different version or release numbers
    repo = INSTALLED_DB

    for path in glob.glob(os.path.join(repo, "*/*")):
        mod = __load_installed_mod(path)
        if mod is not None:
            mods.append(mod)
    return set(mods)


def __load_installed_mod(path):
    if os.path.exists(path):
        files = glob.glob(os.path.join(path, "*.pybuild"))
        if len(files) > 1:
            raise Exception('Multiple versions of mod "{}" installed!'.format(atom))
        elif len(files) == 0:
            return None

        for file in files:
            if __mods.get(file, False):
                return __mods[file]
            else:
                mod = load_file(file)
                if mod == None:
                    continue

                mod.INSTALLED = True
                with open(os.path.join(path, "REPO"), "r") as repo_file:
                    mod.REPO = repo_file.readlines()[0]
                    mod.DISPLAY_ATOM = mod.ATOM + "::" + mod.REPO
                with open(os.path.join(path, "USE"), "r") as use_file:
                    mod.INSTALLED_USE = use_file.readlines()[0].split()

                __mods[file] = mod
                return mod
    return None


# Loads mods from the installed database
def load_installed_mod(atom):
    parse = parse_atom(atom)
    if not parse:
        return None

    repo = INSTALLED_DB

    path = None
    if parse["C"]:
        path = os.path.join(repo, parse["C"], parse["MN"])
    else:
        for dirname in glob.glob(os.path.join(repo, "*")):
            path = os.path.join(repo, dirname, parse["MN"])
            if os.path.exists(path):
                break

    if path is not None:
        mod = __load_installed_mod(path)
    else:
        return None

    if mod is None or not atom_sat(mod.ATOM, atom):
        return None
    else:
        return mod


def load_mod(atom):
    parse = parse_atom(atom)
    if not parse:
        return None

    mods = (
        []
    )  # We will return every single mod matching this name. There may be multiple versions in different repos, as well versions with different version or release numbers
    path = None
    for repo in REPOS:
        if not os.path.exists(repo.location):
            warn(
                "Repository {} does not exist at configured location {}".format(
                    repo.name, repo.location
                )
            )
            print(
                'You might need to run "mwmerge --sync" if this is a remote repository'
            )

        if parse["C"]:
            path = os.path.join(repo.location, parse["C"], parse["MN"])
        else:
            for category in get_categories(repo.location):
                path = os.path.join(repo.location, category, parse["MN"])
                if os.path.exists(path):
                    break

        if path is not None and os.path.exists(path):
            for file in glob.glob(os.path.join(path, "*.pybuild")):
                if __mods.get(file, False):
                    mod = __mods[file]
                else:
                    mod = load_file(file)
                    if mod is None:
                        continue

                    mod.INSTALLED = False
                    __mods[file] = mod
                if atom_sat(mod.ATOM, atom):
                    mods.append(mod)
    return mods


def load_all():
    mods = []
    for repo in REPOS:
        for category in get_categories(repo.location):
            for file in glob.glob(
                os.path.join(repo.location, category, "*", "*.pybuild")
            ):
                if __mods.get(file, False):
                    mod = __mods[file]
                else:
                    mod = load_file(file)
                    if mod is None:
                        continue

                    __mods[file] = mod
                mods.append(mod)
    return mods
