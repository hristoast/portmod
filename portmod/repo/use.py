# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import configparser
from portmod.globals import GLOBAL_USE, PORTMOD_CONFIG_DIR, PORTMOD_CONFIG
from portmod.repo.util import select_mod
import portmod.repo.loader
from portmod.repo.atom import atom_sat
from portmod.repo.flags import get_flags, add_flag, remove_flag

__GLOBAL_USE = ["bloodmoon", "tribunal"]


def get_use(mod):
    use_file = os.path.join(PORTMOD_CONFIG_DIR, "mod.use")

    use = set(GLOBAL_USE).union(get_flags(use_file, mod.ATOM))

    enabled_use = set([x for x in use if not x.startswith("-")])
    disabled_use = set([x for x in use if x.startswith("-")])

    enabled_use = enabled_use.intersection(mod.IUSE) | set(
        [
            x.lstrip("+")
            for x in mod.IUSE
            if x.startswith("+") and x.lstrip("+") not in disabled_use
        ]
    )

    return (enabled_use, disabled_use)


def verify_use(flag, atom=None):
    if atom:
        return any(flag in mod.IUSE for mod in loader.load_mod(atom))
    else:
        return flag in __GLOBAL_USE


def add_use(flag, atom=None, disable=False):
    if disable:
        remove_use(flag, atom)

    disableflag = "-" + flag

    if not verify_use(flag, atom):
        if atom:
            print("Error: {} is not a valid use flag for mod {}".format(flag, atom))
        else:
            print("Error: {} is not a valid global use flag".format(flag))
        return

    if atom is not None:
        (newest, req) = select_mod(loader.load_mod(atom))
        if flag not in newest.IUSE:
            print(
                "Warning: {} is not a valid use flag for {}, the default selected version of mod {}".format(
                    flag, newest.ATOM, atom
                )
            )

    if atom:
        print("Adding flag {} to {}".format(flag, atom))
        use_file = os.path.join(PORTMOD_CONFIG_DIR, "mod.use")
        if disable:
            remove_flag(use_file, atom, flag)
            add_flag(use_file, atom, disableflag)
        else:
            remove_flag(use_file, atom, disableflag)
            add_flag(use_file, atom, flag)
    else:
        mmm_config = configparser.ConfigParser()
        mmm_config.read(PORTMOD_CONFIG)
        GLOBAL_USE = mmm_config["general"].get("USE", "").split()

        if (not disable and flag not in GLOBAL_USE) or (
            disable and disableflag not in GLOBAL_USE
        ):
            if disable:
                print("Adding -{} to USE in portmod.cfg".format(flag))
                GLOBAL_USE.append(disableflag)
            else:
                print("Adding {} to USE in portmod.cfg".format(flag))
                GLOBAL_USE.append(flag)

            mmm_config["general"]["USE"] = " ".join(GLOBAL_USE)

            with open(PORTMOD_CONFIG, "w") as conf:
                mmm_config.write(conf)


def remove_use(flag, atom=None):
    disableflag = "-" + flag

    if not verify_use(flag, atom):
        print("Warning: {} is not a valid use flag for mod {}".format(flag, atom))

    if atom is not None:
        use_file = os.path.join(PORTMOD_CONFIG_DIR, "mod.use")
        remove_flag(use_file, atom, flag)
        remove_flag(use_file, atom, disableflag)
    else:
        mmm_config = configparser.ConfigParser()
        mmm_config.read(PORTMOD_CONFIG)
        GLOBAL_USE = mmm_config["general"].get("USE", "").split()

        if flag in GLOBAL_USE or disableflag in GLOBAL_USE:
            if flag in GLOBAL_USE:
                print("Removing {} from USE in portmod.cfg".format(flag))
                GLOBAL_USE = list(filter(lambda a: a != flag, GLOBAL_USE))

            if disableflag in GLOBAL_USE:
                print("Removing -{} from USE in portmod.cfg".format(flag))
                GLOBAL_USE = list(filter(lambda a: a != disableflag, GLOBAL_USE))

            mmm_config["general"]["USE"] = " ".join(GLOBAL_USE)

            with open(PORTMOD_CONFIG, "w") as conf:
                mmm_config.write(conf)
