# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from enum import Enum
import os
from portmod.repo.download import download, get_filename, get_hash, get_download


class ManifestFile:
    def __init__(self, file):
        self.entries = {}
        self.FILE = file
        if os.path.exists(file):
            with open(file, "r") as manifest:
                for line in manifest.readlines():
                    filetype, name, size, shasum = line.split()
                    self.entries[name] = Manifest(
                        name, FileType[filetype], shasum, size
                    )

    def add_entry(self, entry):
        if entry is None:
            raise Exception("Adding None to manifest")
        ours = self.entries.get(entry.NAME)
        if ours is None or not ours.to_string() == entry.to_string():
            print("Adding {} to Manifest...".format(entry.NAME))
            self.entries[entry.NAME] = entry

    def write(self):
        with open(self.FILE, "w") as manifest:
            lines = [entry.to_string() for entry in self.entries.values()]
            lines.sort()
            for line in lines:
                print(line, file=manifest)

    def get(self, name):
        return self.entries.get(name)


class FileType(Enum):
    PYBUILD = "PYBUILD"
    DIST = "DIST"
    AUX = "AUX"
    MISC = "MISC"


class Manifest:
    def __init__(self, name, filetype, shasum, size):
        self.NAME = name
        if type(filetype) is not FileType:
            raise Exception(
                "filetype {} of manifest entry must be a FileType".format(filetype)
            )
        self.FILE_TYPE = filetype
        self.SHASUM = shasum
        self.SIZE = int(size)

    def to_string(self):
        return "{} {} {} {}".format(
            self.FILE_TYPE.name, self.NAME, self.SIZE, self.SHASUM
        )


def add_to_manifest(file):
    with open(manifest_path(pybuild_file), "w") as manifest:
        manifest.writelines(lines)


# Atomatically download mod DIST files (if not already in cache) and create a manifest file
def create_manifest(mod):
    pybuild_file = mod.FILE
    manifest = ManifestFile(manifest_path(pybuild_file))

    # Add sources to manifest
    for source in mod.SOURCES:
        filename = get_filename(source.NAME)
        if not os.path.exists(filename):
            print("Downloading...")
            download(source.URL, source.NAME)
            print("Downloaded " + filename)

        shasum = get_hash(filename)
        size = os.path.getsize(filename)

        manifest.add_entry(Manifest(source.NAME, FileType.DIST, shasum, size))

    # Add ebuild to manifest
    manifest.add_entry(
        Manifest(
            os.path.basename(pybuild_file),
            FileType.PYBUILD,
            get_hash(pybuild_file),
            os.path.getsize(pybuild_file),
        )
    )

    metadata_file = os.path.join(os.path.dirname(pybuild_file), "metadata.yaml")
    # Add other files to manifest
    if os.path.exists(metadata_file):
        shasum = get_hash(metadata_file)
        manifest.add_entry(
            Manifest(
                "metadata.yaml", FileType.MISC, shasum, os.path.getsize(metadata_file)
            )
        )

    # Write changes to manifest
    manifest.write()


# Verify that files match the manifest for a given mod.
def verify_manifest(mod):
    # TODO: Implement
    pass


def manifest_path(file):
    return os.path.join(os.path.dirname(file), "Manifest")


# Loads the manifest for the given file, i.e. the Manifest file in the same directory
#    and turns it into a map of filenames to (shasum, size) pairs
def get_manifest(file):
    m_path = manifest_path(file)

    return ManifestFile(m_path)


def get_total_download_size(mods):
    download_bytes = 0
    for mod in mods:
        manifest_file = get_manifest(mod.FILE)
        for manifest in manifest_file.entries.values():
            if manifest.FILE_TYPE == FileType.DIST:
                download_bytes += manifest.SIZE

    return "{:.3f} MiB".format(download_bytes / 1024 / 1024)


def get_download_size(mods):
    download_bytes = 0
    for mod in mods:
        manifest_file = get_manifest(mod.FILE)
        for manifest in manifest_file.entries.values():
            if (
                manifest.FILE_TYPE == FileType.DIST
                and get_download(manifest.NAME, manifest.SHASUM) is None
            ):
                download_bytes += manifest.SIZE

    return "{:.3f} MiB".format(download_bytes / 1024 / 1024)
