# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import re


def parse_atom(atom):
    expr = re.compile(
        r"(?P<O>([<>]=?|[<>=]))?((?P<C>[a-z\-]+)/)?(?P<M>(?P<MN>[a-z\-]+)(-(?P<MV>[0-9\.]+))?)(-(?P<MR>r[0-9]+))?(::(?P<R>.*))?(\[(?P<USE>[a-z\-+,]*)\])?$"
    )
    match = expr.match(atom)
    CM = None
    CMN = None
    D = None
    USE = set()
    if not match:
        return None

    if match.group("M") and match.group("C"):
        CM = match.group("C") + "/" + match.group("M")
        D = match.group("C") + "_" + match.group("M")
        CMN = match.group("C") + "/" + match.group("MN")
    if match.group("USE"):
        USE = set(match.group("USE").split(","))

    return {
        "M": match.group("M"),
        "MN": match.group("MN"),
        "MV": match.group("MV"),
        "MR": match.group("MR"),
        "C": match.group("C"),
        "R": match.group("R"),
        "O": match.group("O"),
        "CM": CM,
        "USE": USE,
        "D": D,
        "CMN": CMN,
    }


def is_valid_atom(atom):
    parsed = parse_atom(atom)
    if parsed["O"] != "" and parsed["MV"] == "":
        return False
    return True


def newer_atom(oldatom, newatom):
    old = parse_atom(oldatom)
    new = parse_atom(newatom)
    if old["CMN"] != new["CMN"]:
        raise Exception("Cannot compare versions of two different mods!")

    if old["MV"] == new["MV"]:  # If version numbers are the same
        if (
            old.get("MR") is not None and new.get("MR") is not None
        ):  # If both mods have revisions, return true if the new mod has the larger revision
            return int(old["MR"][1:]) < int(new["MR"][1:])
        elif (
            new.get("MR") is not None
        ):  # If only the new mod has a revision, it must be newer
            return True
        else:  # If nether have revisions, then the versions are the same. If the old one has a revision and the new one doesn't, the old one is newer
            return False
    else:  # Versions are unequal
        oldver = old["MV"].split(".")
        newver = new["MV"].split(".")
        for i in range(0, min([len(oldver), len(newver)])):
            if newver[i] > oldver[i]:
                return True
            elif newver[i] < oldver[i]:
                return False
            # If they are the same, check the next part of the version

        # If we haven't already returned, then both strings match, but one has more dots. Return the mod with the longer version
        return len(oldver) < len(newver)


# Determines if a fully qualified atom (can only refer to a single package) satisfies a generic atom
def atom_sat(fq_atom, atom):
    parfq = parse_atom(fq_atom)
    para = parse_atom(atom)

    if parfq["MN"] != para["MN"]:
        # Mods must have the same name
        return False

    if para["C"] and (para["C"] != parfq["C"]):
        # If para defines category, it must match
        return False

    if para["R"] and (para["R"] != parfq["R"]):
        # If para defines repo, it must match
        return False

    if not para["O"]:
        # Simple atom, either one version or all versions will satisfy

        # Check if version is correct
        if para["MV"] and (parfq["MV"] != para["MV"]):
            return False

        # Check if revision is correct
        if para["MR"] and (parfq["MR"] != para["MR"]):
            return False
    else:
        equal = parfq["MV"] == para["MV"]
        lessthan = newer_atom(fq_atom, atom)
        greaterthan = newer_atom(atom, fq_atom)

        if para["O"] == "=":
            return equal
        elif para["O"] == "<":
            return lessthan
        elif para["O"] == "<=":
            return equal or lessthan
        elif para["O"] == ">":
            return greaterthan
        elif para["O"] == ">=":
            return equal or greaterthan

    return True
