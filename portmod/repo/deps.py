# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import sys
from enum import Enum
from colorama import Fore
from portmod.repo.loader import load_mod, load_all_installed, load_installed_mod
from portmod.repo.atom import atom_sat, parse_atom, newer_atom
from portmod.repo.sets import get_set
from portmod.repo.use import get_use
from portmod.repo.util import select_mod
from portmod.repo.manifest import FileType, get_download_size
from portmod.repo.download import get_download
from portmod.config import read_config, write_config
from portmod.colour import colour
from portmod.globals import GLOBAL_USE
from portmod.tsort import tsort
from portmod.masters import get_masters
from portmod.log import warn, err


class Trans(Enum):
    DELETE = "d"
    NEW = "N"
    UPDATE = "U"
    DOWNGRADE = "D"
    REINSTALL = "R"


# Returns true if new mod object supercedes old mod object and false otherwise
# Raises exception if mods do not match
def newer_version(old, new):
    return newer_atom(old.ATOM, new.ATOM)


def print_transactions(mods, verbose=False, out=sys.stdout, summarize=True):
    download_size = get_download_size([mod for (_, mod) in mods])

    for (trans, mod) in mods:
        enabled_use = mod.USE
        disabled_use = set(
            ["-" + x.lstrip("+") for x in mod.IUSE if x not in enabled_use]
        )

        # Use flags are red when enabled, blue when disabled and green if their state is different from the installed version
        usestrings = []

        for flag in [flag.lstrip("+") for flag in mod.IUSE]:
            v = verbose
            if trans == Trans.DELETE:
                # When deleting a mod, don't display flags
                if not verbose:
                    continue
            elif trans == Trans.NEW:
                # Always print all flags for new mods
                v = True
            else:  # Trans.REINSTALL Trans.UPDATE Trans.DOWNGRADE
                # Print only flags that are different compared to the installed version
                pass

            installed_mod = load_installed_mod(mod.CMN)
            if installed_mod is not None and (
                flag in enabled_use and flag not in installed_mod.INSTALLED_USE
            ):
                usestrings.append(colour(Fore.LIGHTGREEN_EX, flag))
            elif installed_mod is not None and (
                flag not in enabled_use and flag in installed_mod.INSTALLED_USE
            ):
                usestrings.append(colour(Fore.LIGHTGREEN_EX, "-" + flag))
            elif v and flag in enabled_use:
                usestrings.append(colour(Fore.RED, flag))
            elif v:
                usestrings.append(colour(Fore.LIGHTBLUE_EX, "-" + flag))

        oldver = ""

        if trans == Trans.DELETE:
            trans_colour = Fore.RED
        elif trans == Trans.NEW:
            trans_colour = Fore.LIGHTGREEN_EX
        elif trans == Trans.REINSTALL:
            trans_colour = Fore.YELLOW
        elif trans == Trans.DOWNGRADE or trans == Trans.UPDATE:
            trans_colour = Fore.BLUE
            installed_mod = load_installed_mod(mod.CMN)
            oldver = colour(Fore.BLUE, " [" + installed_mod.MV + "]")

        if verbose:
            modstring = mod.DISPLAY_ATOM
        else:
            modstring = mod.ATOM

        if len(usestrings) > 0:
            print(
                '[{}] {}{} USE="{}"'.format(
                    colour(trans_colour, trans.value),
                    colour(Fore.GREEN, modstring),
                    oldver,
                    " ".join(usestrings),
                ),
                file=out,
            )
        else:
            print(
                "[{}] {}{}".format(
                    colour(trans_colour, trans.value),
                    colour(Fore.GREEN, modstring),
                    oldver,
                ),
                file=out,
            )

    if summarize:
        print(
            "Total: {} mods ({} updates, {} new, {} reinstalls), Size of downloads: {}".format(
                len(mods),
                len(
                    [
                        trans
                        for (trans, _) in mods
                        if trans == Trans.UPDATE or trans == Trans.DOWNGRADE
                    ]
                ),
                len([trans for (trans, _) in mods if trans == Trans.NEW]),
                len([trans for (trans, _) in mods if trans == Trans.REINSTALL]),
                download_size,
            ),
            file=out,
        )


def sort_transactions(transactions):
    # Create graph and do a topological sort to ensure that mods are installed/removed in the correct order given their dependencies
    graph = {}
    for (trans, mod) in transactions:
        graph[mod.ATOM] = set()

    for (trans, mod) in transactions:
        depends = set()
        for dep in mod.DEPENDS:
            for (_, othermod) in transactions:
                if atom_sat(othermod.ATOM, dep):
                    depends.add(othermod.ATOM)
        if trans == Trans.DELETE:
            # When removing mods, remove them before their dependencies
            graph[mod.ATOM] |= depends
        else:
            # When adding or updating mods, mods, add or update their dependencies before them
            for dep in depends:
                graph[dep].add(mod.ATOM)

    mergeorder = tsort(graph)

    if mergeorder is None:
        print("ERROR: Cycle in dependency graph!")
        return

    new_trans = []
    for atom in mergeorder:
        for (trans, mod) in transactions:
            if mod.ATOM == atom:
                new_trans.append((trans, mod))
                break

    return new_trans


def sort_config():
    print("Sorting openmw.cfg...")
    # Sort 'data' entries in config
    graph = {}
    priorities = {}

    # Determine all Directories that are enabled
    for mod in load_all_installed():
        graph[mod.CM] = set()
        priorities[mod.CM] = mod.TIER

    # Add edges in the graph for each data override
    for mod in load_all_installed():
        for parent in mod.DATA_OVERRIDES:
            atom = next([key for keys in graph.keys() if atom_sat(parent, key)])
            graph[atom].add(mod.CM)

    sorted_mods = tsort(graph, priorities)
    new_dirs = []

    # Turn the sorted list of atoms into a sorted list of data directories
    for atom in sorted_mods:
        mod = load_installed_mod(atom)
        for install in mod.INSTALL_DIRS:
            if satisfies_uselist(install.USE, mod.INSTALLED_USE):
                path = '"' + mod.get_dir_path(install).rstrip("/.") + '"'
                new_dirs.append(path)

    config = read_config()
    old_dirs = config["data"]
    extraneous = list(filter(lambda x: x not in new_dirs, old_dirs))

    if len(extraneous) > 0:
        print()
        print("\n".join(extraneous))
        warn(
            "The above Data Directories were in openmw.cfg but were not installed by portmod. They have been appended to the end of the data directory list."
        )

    config["data"] = new_dirs + extraneous

    # Sort 'content' entries in config
    # Create graph of content files that are installed, with masters of a file being the parent of the node in the graph
    # Any other content files found in the config should be warned about and removed.
    graph = {}
    priorities = {}

    # Determine all ESPs that are enabled
    for mod in load_all_installed():
        for install in mod.INSTALL_DIRS:
            if satisfies_uselist(install.USE, mod.INSTALLED_USE):
                for esp in install.PLUGINS:
                    if satisfies_uselist(esp.USE, mod.INSTALLED_USE):
                        graph[esp.NAME] = set()
                        priorities[esp.NAME] = mod.TIER

    for mod in load_all_installed():
        for install in mod.INSTALL_DIRS:
            if satisfies_uselist(install.USE, mod.INSTALLED_USE):
                for esp in install.PLUGINS:
                    if satisfies_uselist(esp.USE, mod.INSTALLED_USE):
                        # We need a path to determine masters
                        path = mod.get_file_path(install, esp)
                        masters = get_masters(path)

                        # Add edge from master to child
                        for master in masters:
                            if graph.get(master) is None:
                                raise Exception(
                                    "Missing master {} for File {}".format(
                                        master, esp.NAME
                                    )
                                )
                            graph[master].add(esp.NAME)

    if "content" in config:
        new_esps = tsort(graph, priorities)
        old_esps = config["content"]
        extraneous = list(filter(lambda x: x not in new_esps, old_esps))

        if len(extraneous) > 0:
            print()
            print("\n".join(extraneous))
            warn(
                "The above Content files were in openmw.cfg but were not installed by portmod. They have been appended to the end of the load list."
            )

        config["content"] = new_esps + extraneous

    graph = {}
    priorities = {}
    for mod in load_all_installed():
        for install in mod.INSTALL_DIRS:
            if satisfies_uselist(install.USE, mod.INSTALLED_USE):
                for bsa in install.ARCHIVES:
                    if satisfies_uselist(bsa.USE, mod.INSTALLED_USE):
                        graph[bsa.NAME] = set()
                        priorities[bsa.NAME] = mod.TIER

    if "fallback-archive" in config:
        new_bsas = tsort(graph, priorities)
        old_bsas = config["fallback-archive"]
        extraneous = list(filter(lambda x: x not in new_bsas, old_bsas))
        if len(extraneous) > 0:
            print()
            print("\n".join(extraneous))
            warn(
                "The above Fallback Archives were in openmw.cfg but were not installed by portmod. They have been appended to the end of the load list."
            )

        config["fallback-archive"] = new_bsas + extraneous
    write_config(config)


def satisfies_uselist(uselist, enabled):
    if len(uselist) == 0:
        return True

    for flag in uselist:
        if flag.startswith("-") and flag.lstrip("-") in enabled:
            return False
        elif not flag.startswith("-") and flag not in enabled:
            return False

    return True


# Determines if the uselist is satisfied for the mod in the current configuration
def satisfies_use(uselist, mod):
    return satisfies_uselist(uselist, mod.USE)


def is_selected(atom):
    selected = get_set("selected")
    for selatom in selected:
        if atom_sat(atom, selatom):
            return True
    return False


# Takes a list of mods and Returns a set of installed mods that are dependent on the mods in the input list
def find_dependent(mods):
    installed = load_all_installed()
    dependent = set()

    for installed_mod in installed:
        for mod in mods:
            for atom in installed_mod.DEPENDS:
                if atom_sat(mod.ATOM, atom):
                    dependent.add(installed_mod)
                    break
    return dependent


# Takes a list of transactions and finds transactions that need to be done to to handle the dependencies of the input transactions.
# Returned list includes the original transactions
def find_dependencies(transactions):
    changes = []
    installed = {}
    for (trans, mod) in transactions:
        installed[mod.ATOM] = mod

    for mod in load_all_installed():
        if mod.ATOM not in installed:
            installed[mod.ATOM] = mod

    worklist = transactions.copy()
    new_mods = transactions.copy()
    while len(worklist) > 0:
        (trans, mod) = worklist.pop()

        for atom in mod.DEPENDS:
            usedeps = parse_atom(atom)["USE"]
            enabled_use = set([x for x in usedeps if not x.startswith("-")])
            disabled_use = set([x[1:] for x in usedeps if x.startswith("-")])

            # Check if atom is satisfied by a mod in the installed set
            if trans != Trans.DELETE:
                satisfied = False
                for i in installed.values():
                    if atom_sat(i.ATOM, atom):
                        (_, disabled) = get_use(i)
                        enabled = i.USE
                        if enabled_use <= enabled and not any(
                            k in enabled for k in disabled_use
                        ):
                            satisfied = True
                            break
                        else:
                            # TODO: Automatic use flag resolution when possible, prompting the user to accept the suggested use changes
                            print(
                                "ERROR: USE flags {} not satisfied for dependency {}".format(
                                    usedeps - i.USE, atom
                                )
                            )
                            return ([], [])
            else:
                # Check if there are any mods dependent on this dependency, and remove it if not
                if is_selected(atom):
                    continue

                # Satisfied in this case means that the dependency is not installed, or is required by other mods
                satisfied = True
                for i in installed.values():
                    if atom_sat(i.ATOM, atom):
                        satisfied = False
                        dependency = i
                        break

                # If mod is installed, we only want to remove it if no installed mods depend on it
                if not satisfied:
                    satisfied = (
                        len(
                            [
                                x
                                for x in find_dependent([dependency])
                                if x not in [mod for (trans, mod) in new_mods]
                            ]
                        )
                        != 0
                    )

            if not satisfied:
                # find mod that satisfies atom and add to mods
                pending_mods = load_mod(atom)
                if len(pending_mods) > 0:
                    (new_mod, dep) = select_mod(pending_mods)
                    changes.append(dep)

                    if trans == Trans.DELETE:
                        new_mods.append((trans, new_mod))
                    else:
                        new_mods.append((trans, new_mod))
                    installed[new_mod.ATOM] = new_mod
                    worklist.append((trans, new_mod))
                else:
                    print("ERROR: unable to satisfy atom {}".format(atom))
                    return ([], [])

    # Check transaction list for conflicting mods
    for mod1 in load_all_installed() | set([mod for (_, mod) in new_mods]):
        for mod2 in load_all_installed() | set([mod for (_, mod) in new_mods]):
            for atom in mod1.CONFLICTS:
                if mod1 != mod2 and atom_sat(mod2.ATOM, atom):
                    err(
                        "Mod {} conflicts with {}".format(
                            mod2.DISPLAY_ATOM, mod1.DISPLAY_ATOM
                        )
                    )
                    return ([], [])

    return (new_mods, changes)
