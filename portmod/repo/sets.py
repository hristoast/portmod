# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from portmod.globals import SET_DIR


def get_set(mod_set):
    set_file = os.path.join(SET_DIR, mod_set)
    if os.path.exists(set_file):
        with open(set_file, "r") as file:
            return set(file.read().splitlines())
    else:
        return set()


def add_set(mod_set, atom):
    set_file = os.path.join(SET_DIR, mod_set)
    os.makedirs(SET_DIR, exist_ok=True)
    if os.path.exists(set_file):
        with open(set_file, "r+") as file:
            for line in file:
                if atom in line:
                    break
            else:
                print(atom, file=file)
    else:
        with open(set_file, "a+") as file:
            print(atom, file=file)


def remove_set(mod_set, atom):
    set_file = os.path.join(SET_DIR, mod_set)
    if os.path.exists(set_file):
        with open(set_file, "r+") as f:
            new_f = f.readlines()
            f.seek(0)
            for line in new_f:
                if atom not in line:
                    f.write(line)
            f.truncate()
