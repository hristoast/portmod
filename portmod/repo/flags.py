# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import configparser
from portmod.repo.atom import atom_sat


def get_flags(file, atom):
    flags = []
    if os.path.exists(file):
        with open(file, mode="r") as flagfile:
            for line in flagfile.readlines():
                elem = line.split()
                flags.append((elem[0], elem[1:]))
    else:
        return set()

    atom_flags = set()
    for (a, flaglist) in flags:
        if atom_sat(atom, a):
            atom_flags = atom_flags.union(set(flaglist))

    return set(atom_flags)


def add_flag(file, atom, flag):
    if os.path.exists(file):
        flagfile = __read_flags(file)
    else:
        flagfile = []

    found = False
    for (index, line) in enumerate(flagfile):
        l = line.split()
        if l[0] == atom:
            if flag not in l:
                print("Adding flag {} to {} in {}".format(flag, atom, file))
                file[index] = "{} {}".format(line, flag)
            found = True
    if not found:
        print("Adding flag {} to {} in {}".format(flag, atom, file))
        flagfile.append("{} {}".format(atom, flag))

    __write_flags(file, flagfile)


def remove_flag(file, atom, flag):
    flagfile = __read_flags(file)

    for (index, line) in enumerate(flagfile):
        l = line.split()
        if atom_sat(l[0], atom) and flag in l:
            print("Removing flag {} from {} in {}".format(flag, atom, file))
            l = list(filter(lambda a: a != flag, l))

            if len(l) > 1:
                flagfile[index] = " ".join(l)
            else:
                del flagfile[index]

    __write_flags(flagfile)


def __read_flags(file):
    with open(file, mode="r") as flagfile:
        return flagfile.read().splitlines()


def __write_flags(file, new_flagfile):
    with open(file, mode="w") as flagfile:
        for line in new_flagfile:
            print(line, file=flagfile)
