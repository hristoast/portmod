# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

# Helper functions for interacting with repository metadata files.
# Arguments are either a repo path, or mod object.

import os
import yaml
from portmod.globals import ACCEPT_LICENSE
from portmod.repo.list import read_list
from portmod.repos import REPOS
from portmod.log import err


class Person(yaml.YAMLObject):
    yaml_tag = "!person"

    def __init__(self, email, desc=""):
        self.email = email
        self.desc = desc

    def __repr__(self):
        return "%s(email=%r, desc=%r)" % (
            self.__class__.__name__,
            self.email,
            self.desc,
        )


class Group(yaml.YAMLObject):
    yaml_tag = "!group"

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "%s(name=%r)" % (self.__class__.__name__, self.name)


def get_masters(repo_path):
    masters = []
    for repo in REPOS:
        if os.path.abspath(os.path.normpath(repo_path)) == os.path.abspath(
            os.path.normpath(repo.location)
        ):
            for master in repo.masters:
                n = next((x for x in REPOS if x.name == master), None)
                if n is not None:
                    masters.append(n)
                else:
                    err("Unable to find master {} of repo {}".format(master, repo.name))
    return masters


def get_repo_name(path):
    root = get_repo_root(path)
    if root is None:
        return None
    path = os.path.join(root, "metadata", "repo_name")
    if os.path.exists(path):
        with open(path, mode="r") as name_file:
            return name_file.read().strip()
    return None


def get_repo_root(path):
    path = os.path.abspath(path)
    # Recursively look for metadata/repo_name to identify root
    if os.path.exists(os.path.join(path, "metadata", "repo_name")):
        return path
    elif os.path.dirname(path) == path:
        # We've reached the root of the FS there is no repo
        return None

    return get_repo_root(os.path.dirname(path))


# Retrieves the list of categories given a path to a repo
def get_categories(repo):
    categories = set()
    path = os.path.join(repo, "metadata", "categories.list")
    if os.path.exists(path):
        categories |= set(read_list(path))
    for master in get_masters(repo):
        categories |= get_categories(master.location)

    return categories


def get_archs(repo):
    archs = set()
    path = os.path.join(repo, "metadata", "arch.list")
    if os.path.exists(path):
        archs |= set(read_list(path))

    for master in get_masters(repo):
        archs |= get_archs(master.location)

    return read_list(path)


def get_global_use(repo):
    use = {}
    path = os.path.join(repo, "metadata", "use.yaml")
    if os.path.exists(path):
        with open(path, mode="r") as use_file:
            use.update(yaml.load(use_file))

    for master in get_masters(repo):
        use.update(get_global_use(master.location))

    return use


# Returns true if the given license is valid
def license_exists(repo, name):
    path = os.path.join(repo, "licenses", name)
    if os.path.exists(path):
        return True

    for master in get_masters(repo):
        if license_exists(master.location, name):
            return True

    return False


def has_eula(mod):
    groups = get_license_groups(get_repo_root(mod.FILE))
    return mod.LICENSE in groups.get("EULA")


# returns the full content of the named license
def get_license(repo, name):
    path = os.path.join(repo, "licenses", name)
    if os.path.exists(path):
        with open(path, mode="r") as license_file:
            return license_file.read()
    else:
        for master in get_masters(repo):
            license = get_license(master.location, name)
            if license is not None:
                return license

        err("Nonexistant license: {}".format(name))
        return None


# returns a map of group names to sets of licenses
def get_license_groups(repo):
    result = {}
    path = os.path.join(repo, "metadata", "license_groups.yaml")
    if os.path.exists(path):
        with open(path, mode="r") as group_file:
            groups = yaml.load(group_file)

        for name, values in groups.items():
            if values is not None:
                result[name] = set(values.split())
            else:
                result[name] = set()

    for master in get_masters(repo):
        result.update(get_license_groups(master.location))

    return result


# returns true if the user's ACCEPTS_LICENSE accepts the given mod
# For a license to be accepted, it must be both listed, either explicitly, part of a group, or with the * wildcard
#   and it must not be blacklisted by a license or license group prefixed by a -
def is_license_accepted(mod):
    name = mod.LICENSE
    license_groups = get_license_groups(get_repo_root(mod.FILE))

    allowed = False
    # Check if license is allowed by anything in ACCEPT_LICENSE
    for license in ACCEPT_LICENSE:
        if license.startswith("-") and (
            license == name
            or (license[2] == "@" and name in license_groups[license[2:]])
        ):
            # not allowed if matched by this
            return False
        if license == "*":
            allowed = True
        if license.startswith("@") and name in license_groups[license[1:]]:
            allowed = True

    return allowed
    # TODO: implement mod-specific license acceptance via mod.license config file


# Loads the metadata file for the given mod
def get_mod_metadata(mod):
    path = os.path.join(os.path.dirname(mod.FILE), "metadata.yaml")
    if not os.path.exists(path):
        return None

    with open(path, mode="r") as metadata_file:
        metadata = yaml.load(metadata_file)

    return metadata


# Loads the metadata file for the given category
def get_category_metadata(repo, category):
    path = os.path.join(repo, category, "metadata.yaml")

    if os.path.exists(path):
        with open(path, mode="r") as metadata_file:
            metadata = yaml.load(metadata_file)
        return metadata

    for master in get_masters(repo):
        metadata = get_category_metadata(master.location, category)
        if metadata is not None:
            return metadata

    return None
