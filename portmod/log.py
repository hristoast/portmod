# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from portmod.colour import colour
from colorama import Fore

ERROR = False  # Will be set to true if an error is logged at any time


def warn(string):
    print("{}: {}".format(colour(Fore.YELLOW, "WARNING"), string))


def err(string):
    ERROR = True
    print("{}: {}".format(colour(Fore.RED, "ERROR"), string))
