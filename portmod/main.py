# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import sys
import os
import shutil
import re
import argparse
import fileinput
import fnmatch
import tempfile
import subprocess
import shlex
from pathlib import Path
from itertools import chain
import configparser
import git
import json
import io
from portmod.log import warn, err
from portmod.globals import (
    TMP_DIR,
    MOD_DIR,
    OPENMW_CONFIG,
    OPENMW_CONFIG_DIR,
    INSTALLED_DB,
)
from portmod.repos import REPOS
import portmod.repo
from portmod.repo.loader import load_file, load_mod, load_installed_mod
from portmod.repo.deps import (
    find_dependent,
    find_dependencies,
    print_transactions,
    Trans,
    satisfies_use,
    sort_transactions,
    sort_config,
    newer_version,
)
from portmod.repo.use import add_use, remove_use
from portmod.repo.util import select_mod, KeywordDep, LicenseDep, get_hash
from portmod.config import (
    add_config,
    find_config,
    find_config_subdirs,
    remove_config,
    remove_config_subdirs,
    check_config,
    check_config_subdirs,
    read_config,
    write_config,
)
from portmod.repo.sets import add_set, remove_set, get_set
from portmod.repo.keywords import add_keyword
from portmod.colour import colour
from portmod.mod import install_mod, remove_mod
from portmod.prompt import prompt_bool
from portmod.importer import import_mod
from portmod.repo.manifest import create_manifest, get_manifest, Manifest, FileType
from portmod.progress import wait_subprocess
from portmod.query import query_installed, query_insensitive, display_search_results
from colorama import Fore


def parse_args():
    parser = argparse.ArgumentParser(
        description="Command line interface to manage OpenMW mods"
    )
    parser.add_argument("mods", metavar="MOD", help="Mods to install", nargs="*")
    parser.add_argument(
        "--import",
        help="automatically generates pybuilds for mods specified in the given file. File should consist a mod atom and url per line, separated by a space",
        dest="import_mods",
    )
    parser.add_argument(
        "--sync", help="Fetches and updates repository", action="store_true"
    )
    parser.add_argument(
        "-c",
        "--depclean",
        help="Removes mods and their dependencies. Will also remove mods dependent on the given mods",
        action="store_true",
    )
    parser.add_argument(
        "-C",
        "--delete",
        help="Removes the given mods without checking dependencies.",
        action="store_true",
    )
    parser.add_argument(
        "--no-confirm",
        help="Doesn't ask for confirmation when installing or removing mods",
        action="store_true",
    )
    parser.add_argument(
        "-1",
        "--oneshot",
        help="Do not make any changes to the selected set when instaling or removing mods",
        action="store_true",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Print extra information. Currently shows mod repository and all use flag states, rather than just changed use flags",
        action="store_true",
    )

    parser.add_argument(
        "-u",
        "--update",
        help="Updates mods to the best version available and excludes packages if they are already up to date.",
        action="store_true",
    )
    # TODO: Implement
    parser.add_argument(
        "-N",
        "--new-use",
        help="Includes mods whose use flags have changed since they were last installed [unimplemented]",
        action="store_true",
    )
    # TODO: Implement
    parser.add_argument(
        "-D",
        "--deep",
        help="Consider entire dependency tree when doing dependency resolution instead of just the immediate dependencies [unimplemented]",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--search",
        help="Searches the repository for mods with the given phrase in their name",
        action="store_true",
    )
    parser.add_argument(
        "-S",
        "--searchdesc",
        help="Searches the repository for mods with the given phrase in their name or description",
        action="store_true",
    )
    # TODO: Ensure that installed mods database matches mods that are actually installed
    parser.add_argument(
        "--validate",
        help="Checks if the mods in the mod directory are installed, and that the directories in the config all exist",
        action="store_true",
    )
    parser.add_argument(
        "--sort-config",
        help="Sorts the config. This is for debugging purposes, as the config is normally sorted as necessary.",
        action="store_true",
    )

    if len(sys.argv) == 1:
        parser.print_help()

    return parser.parse_args()


def cleanup():
    if os.path.exists(TMP_DIR):
        shutil.rmtree(TMP_DIR)


def mods_not_in_config(config):
    mods = []
    for category in os.listdir(MOD_DIR):
        for directory in os.listdir(MOD_DIR):
            path = os.path.abspath(
                os.path.normpath(os.path.join(MOD_DIR, category, directory))
            )
            if os.path.isdir(path) and not directory.startswith("."):
                if check_config_subdirs(config, "data", path) == -1:
                    mods.append(path)
    return mods


def nonexistant_mods(config):
    directories = [path for (index, path) in find_config(config, "data", "*")]
    return filter(lambda x: not os.path.exists(x), directories)


def mods_not_in_moddir(config):
    directories = [path for (index, path) in find_config(config, "data", "*")]
    return filter(
        lambda x: not x.startswith(MOD_DIR) and x.find("/Morrowind/") == -1, directories
    )


def configure_mods(atoms, delete, depclean, no_confirm, oneshot, verbose, update):
    # Load mod files for the given mods
    transactions = []
    new_selected = []
    satisfied = True
    dep_changes = []
    for mod in atoms:
        if mod.startswith("@"):
            # Atom is actually a set. Load set instead
            atoms.extend(get_set(mod.replace("@", "")))
            continue

        pending_mods = load_mod(mod)

        if pending_mods is None:
            print(
                "{}: invalid atom {}".format(
                    colour(Fore.RED, "ERROR"), colour(Fore.GREEN, mod)
                )
            )
            return
        elif len(pending_mods) > 0:
            (to_install, dep) = select_mod(pending_mods)
            if dep is not None:
                dep_changes.append(dep)

            installed = load_installed_mod(mod)

            if update and installed is not None:
                if newer_version(installed, to_install):
                    transactions.append((Trans.UPDATE, to_install))
            else:
                new_selected.append(to_install)
                if delete or depclean:
                    to_remove = load_installed_mod(mod)
                    if to_remove is not None:
                        transactions.append((Trans.DELETE, to_remove))
                    else:
                        warn(
                            'trying to remove mod "{}", which is not actually installed. Skipping...'.format(
                                colour(Fore.GREEN, mod)
                            )
                        )
                elif installed is not None and installed.ATOM == to_install.ATOM:
                    transactions.append((Trans.REINSTALL, to_install))
                else:
                    transactions.append((Trans.NEW, to_install))
        elif not update:  # When updating, allow mods that are no longer in the repo
            print("Unable to find mod for atom {}".format(mod))
            return

    # Find dependencies for mods and build list of mods to install (or uninstall depending on other arguments)
    if delete:
        # Do nothing. We don't care about deps
        pass
    elif depclean:
        dependent = find_dependent([mod for (trans, mod) in transactions])
        (dependencies, _) = find_dependencies(transactions)
        transactions = list(dependent)
        transactions.extend(dependencies)
    else:
        (transactions, changes) = find_dependencies(transactions)
        dep_changes.extend(changes)

    transactions = sort_transactions(transactions)

    if len(dep_changes) > 0:
        keyword_changes = list(filter(lambda x: type(x) is KeywordDep, dep_changes))
        license_changes = list(filter(lambda x: type(x) is LicenseDep, dep_changes))
        if len(keyword_changes) > 0:
            print(
                'The following keyword changes are necessary to proceed.\nThis will enable enable the installation of a mod that is unstable (if keyword is prefixed by a "~"), or untested, (if keyword is "**")'
            )
            for keyword in keyword_changes:
                if keyword.keyword.startswith("*"):
                    c = Fore.RED
                else:
                    c = Fore.YELLOW
                print(
                    "    {} {}".format(
                        colour(Fore.GREEN, keyword.atom), colour(c, keyword.keyword)
                    )
                )

            if prompt_bool("Would you like to automatically apply these changes?"):
                for keyword in keyword_changes:
                    add_keyword(keyword.atom, keyword.keyword)
            else:
                return

        if len(license_changes) > 0:
            # TODO: For EULA licenses, display the license and prompt the user to accept it.
            print(
                "The following licence changes are necessary to proceed. Please review these licenses and make the changes manually."
            )
            for license in license_changes:
                print(
                    "    {} {}".format(
                        colour(Fore.GREEN, license.atom), colour(c, license.license)
                    )
                )

    # Inform user of change and prompt
    if not no_confirm and len(transactions) > 0:
        if delete or depclean:
            print("These are the mods to be removed, in order:")
            print_transactions(transactions, verbose=verbose)
            print()
            if not (no_confirm or prompt_bool("Would you like to remove these mods?")):
                return
        else:
            print("These are the mods to be installed, in order:")
            print_transactions(transactions, verbose=verbose)
            print()
            if not (no_confirm or prompt_bool("Would you like to install these mods?")):
                return
    elif len(transactions) == 0:
        print("Nothing to do.")
        return

    error = None
    merged = []
    # Install (or remove) mods in order
    for (trans, mod) in transactions:
        if trans == Trans.DELETE:
            remove_mod(mod)
            if not oneshot:
                remove_set("selected", mod.CMN)
            merged.append((trans, mod))
        elif install_mod(mod):
            if mod in new_selected and not oneshot:
                add_set("selected", mod.CMN)
            merged.append((trans, mod))
        else:
            # Unable to install mod. Aborting installing remaining mods
            error = mod.DISPLAY_ATOM
            break

    # Commit changes to installed database
    gitrepo = git.Repo.init(INSTALLED_DB)
    try:
        gitrepo.head.commit
    except ValueError:
        gitrepo.git.commit(m="Initial Commit")

    transstring = io.StringIO()
    print_transactions(merged, True, transstring, False)
    if len(gitrepo.head.commit.diff("HEAD")) > 0:
        # There was an error. We report the mods that were successfully merged and note that an error occurred,
        # however we still commit anyway.
        if error:
            gitrepo.git.commit(
                m="Successfully merged {} mods. Error occurred when attempting to merge {}\n{}".format(
                    len(transactions), error, transstring.getvalue()
                )
            )
        else:
            gitrepo.git.commit(
                m="Successfully merged {} mods: \n{}".format(
                    len(transactions), transstring.getvalue()
                )
            )

    if not error:
        # Check if mods need to be added to rebuild set
        if plugin_changed(transactions):
            for mod in query_installed("REBUILD", "ANY_PLUGIN"):
                if mod.ATOM not in [mod.ATOM for (trans, mod) in transactions]:
                    add_set("rebuild", mod.CMN)

        # Check if mods were just rebuilt and can be removed from the rebuild set
        for mod in query_installed("REBUILD", "ANY_PLUGIN"):
            if mod.ATOM in [mod.ATOM for (trans, mod) in transactions]:
                remove_set("rebuild", mod.CMN)

        if len(get_set("rebuild")) > 0:
            warn("The following mods need to be rebuilt:")
            for atom in get_set("rebuild"):
                print("    {}".format(colour(Fore.GREEN, atom)))
            print(
                "Use {} to rebuild these mods".format(
                    colour(Fore.LIGHTGREEN_EX, "mwmerge @rebuild")
                )
            )

        # Fix ordering in openmw.cfg
        sort_config()


def plugin_changed(transactions):
    for (trans, mod) in transactions:
        for idir in mod.INSTALL_DIRS:
            for plug in idir.PLUGINS:
                if satisfies_use(plug.USE, mod) and satisfies_use(idir.USE, mod):
                    return True


def main():
    args = parse_args()

    if os.path.exists(TMP_DIR):
        if args.no_confirm or prompt_bool(
            "Tempdir {} already exists, possibly due to the script exiting prematurely. Remove?".format(
                TMP_DIR
            )
        ):
            shutil.rmtree(TMP_DIR)

    if args.validate:
        config = read_config()
        for path in nonexistant_mods(config):
            warn(
                '"{}" does not exist, but is referenced in the OpenMW config file!'.format(
                    path
                )
            )

        for path in mods_not_in_config(config):
            warn(
                'Directory "{}" is in mod directory, but not used in the OpenMW config file!'.format(
                    path
                )
            )

    if args.sync:
        for repo in REPOS:
            if repo.auto_sync and repo.sync_type == "git":
                if os.path.exists(repo.location):
                    print("Syncing repo {}...".format(repo.name))
                    gitrepo = git.Repo.init(repo.location)
                    current = gitrepo.head.commit

                    # Remote location has changed. Update gitrepo to match
                    if gitrepo.remotes.origin.url != repo.sync_uri:
                        gitrepo.remotes.origin.set_url(repo.sync_uri)

                    gitrepo.remotes.origin.fetch()
                    gitrepo.head.reset("origin/master", True, True)

                    for diff in current.diff("HEAD"):
                        if diff.renamed_file:
                            print(
                                "{} {} -> {}".format(
                                    diff.change_type, diff.a_path, diff.b_path
                                )
                            )
                        if diff.deleted_file:
                            print("{} {}".format(diff.change_type, diff.a_path))
                        else:
                            print("{} {}".format(diff.change_type, diff.b_path))
                    print("Done syncing repo {}.".format(repo.name))
                else:
                    git.Repo.clone_from(repo.sync_uri, repo.location)
                    print("Initialized Repository")
            elif repo.auto_sync:
                err(
                    'Sync type "{}" for repo "{}" is not supported. Supported types are: git'.format(
                        repo.sync_type, repo.name
                    )
                )

    if args.search:
        mods = None
        for word in args.mods:
            if mods is None:
                mods = query_insensitive("NAME", word)
            else:
                mods ^= query_insensitive("NAME", word)

        display_search_results(mods)
        return

    if args.searchdesc:
        mods = None
        for word in args.mods:
            if mods is None:
                mods = query_insensitive("NAME", word) | query_insensitive("DESC", word)
            else:
                new = query_insensitive("NAME", word) | query_insensitive("DESC", word)
                mods ^= new

        display_search_results(mods)
        return

    if args.mods:
        configure_mods(
            args.mods,
            args.delete,
            args.depclean,
            args.no_confirm,
            args.oneshot,
            args.verbose,
            args.update,
        )
        cleanup()

    if args.import_mods:
        (mod_name, ext) = os.path.splitext(os.path.basename(args.import_mods))

        with open(args.import_mods, mode="r") as file:
            if ext == ".json":
                mods = json.load(file)
                for mod in mods:
                    import_mod(mod)
            else:
                for line in file.readlines():
                    words = line.split()
                    if len(words) > 0:
                        import_mod({"atom": words[0], "url": words[1]})
        cleanup()

    if args.sort_config:
        sort_config()


def pybuild_validate(file_name):
    # Verify that pybuild is valid python
    import py_compile

    py_compile.compile(file_name)

    # Verify fields of pybuild
    try:
        mod = load_file(file_name)
        mod.validate()
    except Exception as e:
        err(str(e))
        err("Unable to load mod {}!".format(file_name))
        return


def pybuild_manifest(file_name):
    if not os.path.exists(file_name):
        err("Pybuild file {} does not exist".format(file_name))
        return

    shasum = get_hash(file_name)
    size = os.path.getsize(file_name)
    # Start by storing the pybuild in the manifest so that we can load it
    manifest = get_manifest(file_name)
    manifest.add_entry(
        Manifest(os.path.basename(file_name), FileType.PYBUILD, shasum, size)
    )
    manifest.write()

    mod = load_file(file_name)
    if mod is None:
        exit(1)

    create_manifest(mod)
