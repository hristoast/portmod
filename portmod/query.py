from operator import attrgetter
from colorama import Fore, Style

from portmod.colour import green, bright
from portmod.repo.manifest import get_total_download_size
from portmod.repo.loader import load_all, load_all_installed, load_installed_mod
from portmod.repo.util import get_newest_mod


def query(field, value):
    return set([mod for mod in load_all() if value in getattr(mod, field)])


def query_insensitive(field, value):
    return set(
        [mod for mod in load_all() if value.lower() in getattr(mod, field).lower()]
    )


def query_installed(field, value):
    return set([mod for mod in load_all_installed() if value in getattr(mod, field)])


def display_search_results(mods):
    groupedmods = {}
    for mod in mods:
        if groupedmods.get(mod.CMN) is None:
            groupedmods[mod.CMN] = [mod]
        else:
            groupedmods[mod.CMN].append(mod)

    sortedgroups = sorted(groupedmods.values(), key=lambda group: group[0].NAME)

    for group in sortedgroups:
        newest = get_newest_mod(group)
        installed = load_installed_mod(newest.CMN)
        download = get_total_download_size([newest])

        if installed is not None:
            installed_str = installed.MV
        else:
            installed_str = "not installed"

        print(
            "{}  {}".format(green("*"), bright(newest.CMN)),
            "       {} {}".format(green("Latest version available:"), newest.MV),
            "       {} {}".format(green("Installed version:       "), installed_str),
            "       {} {}".format(green("Size of files:"), download),
            "       {} {}".format(green("Homepage:"), newest.HOMEPAGE),
            "       {} {}".format(green("Description:"), newest.DESC),
            "       {} {}".format(green("License:"), " ".join(newest.LICENSE)),
            sep="\n",
        )

        print()

    print("\nMods found: {}".format(len(sortedgroups)))
