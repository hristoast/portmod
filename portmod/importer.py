# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import fnmatch
import patoolib
import string
import shutil
import subprocess
import re
from operator import itemgetter
from portmod.repo.download import download, get_hash, get_filename
from portmod.repos import Repo, REPOS, REPOS_FILE
from portmod.globals import TMP_DIR, PORTMOD_LOCAL_DIR
from portmod.repo.atom import parse_atom
from portmod.repo.loader import load_all, load_file
from portmod.colour import colour
from portmod.repo.manifest import create_manifest, get_manifest, Manifest, FileType
from portmod.masters import get_masters
from colorama import Fore

USER_REPO = Repo(
    "user-repo",
    os.path.join(PORTMOD_LOCAL_DIR, "user-repo"),
    False,
    None,
    None,
    "openmw",
    50,
)

# Detect masters using tes3cmd and determine which atom they correspond to
def get_esp_deps(file, datadirs):
    masters = get_masters(file)

    use = set()
    # Check if masters correspond to Tribunal.esm or Bloodmoon.esm, as they are pulled in with global use flags instead of a mod
    if "Tribunal.esm" in masters:
        use.add("tribunal")
        masters.remove("Tribunal.esm")

    if "Bloodmoon.esm" in masters:
        use.add("bloodmoon")
        masters.remove("Bloodmoon.esm")

    atoms = set()
    # For other masters, look for a reference to them in the repository and get the atom
    mods = load_all()
    for mod in mods:
        for data_dir in mod.INSTALL_DIRS:
            for esp in data_dir.PLUGINS:
                if esp.NAME in masters:
                    atoms.add(mod.C + "/" + mod.MN)
                    masters.remove(esp.NAME)

    for data_dir in datadirs:
        (esps, _) = find_esp_bsa(data_dir)
        print(data_dir, esps)
        for file in esps:
            if file in masters:
                masters.remove(file)
    if len(masters) > 0:
        raise Exception(
            '{}: Could not find master(s) "{}" for file: "{}"'.format(
                colour(Fore.RED, "ERROR"), '" "'.join(masters), os.path.basename(file)
            )
        )
    else:
        return (atoms, use)


# Imports a mod decription dictionary.
# Required Fields: url, atom
# Optional Fields: name, desc, homepage, tags
def import_mod(mod):
    todostr = "TODO: FILLME"
    atom = mod["atom"]
    url = mod["url"]
    name = mod.get("name", todostr)
    desc = mod.get("desc", todostr)
    homepage = mod.get("homepage", todostr)
    tags = mod.get("tags", todostr)

    print("Importing {}...".format(atom))
    (C, M, MN, D) = itemgetter("C", "M", "MN", "D")(parse_atom(atom))
    download_name = D
    if not os.path.exists(get_filename(download_name)):
        download(url, download_name)

    # Extract file into tmp
    outdir = os.path.join(TMP_DIR, download_name)
    os.makedirs(outdir)
    patoolib.extract_archive(get_filename(download_name), outdir=outdir)

    # Search for data directories
    data_dirs = find_data_dirs(outdir)
    print("Detected the following data directories {}".format(data_dirs))

    INSTALL_DIRS = []

    dir_data = {}
    dep_atoms = []
    dep_uses = []

    prepare = ""
    includes = ""

    for directory in data_dirs:
        (esps, bsas) = find_esp_bsa(os.path.join(outdir, directory))
        if len(bsas) > 0:
            bsa_string = ',ARCHIVES=["{}"]'.format(
                '","'.join(['File("{}")'.format(bsa) for bsa in bsas])
            )
        else:
            bsa_string = ""

        d_esps = []
        # Get dependencies for the ESP. List dependencies common to all ESPs as deps for the mod, others
        for esp in esps:
            (dep_atom, dep_use) = get_esp_deps(
                os.path.join(outdir, directory, esp),
                [os.path.join(outdir, data_dir) for data_dir in data_dirs],
            )
            print(
                'Found esp "{}" with deps of: {}'.format(esp, dep_atom.union(dep_use))
            )
            d_esps.append({"esp": esp, "dep_atom": dep_atom, "dep_use": dep_use})
            dep_atoms.append(dep_atom)
            dep_uses.append(dep_use)

            if "TR_Data.esm" in get_masters(esp):
                if len(prepare) == 0:
                    prepare += "\n    def prepare(self):\n"
                prepare += "        tr_patcher({})\n".format(
                    os.path.join(outdir, directory, esp)
                )
                includes += ",tr_patcher"

        dir_data[directory] = {"bsas": bsa_string, "esps": d_esps}

    if len(dep_uses) > 0:
        dep_use = set.intersection(*dep_uses)
    else:
        dep_use = set()

    if len(dep_atoms) > 0:
        dep_atom = set.intersection(*dep_atoms)
    else:
        dep_atom = set()

    if "base/morrowind" in dep_atom and len(dep_use) > 0:
        dep_atom.remove("base/morrowind")
        dep_atom.add("base/morrowind[" + ",".join(dep_use) + "]")

    deps = " ".join(dep_atom)

    for directory in data_dirs:
        d = dir_data[directory]
        bsa_string = d["bsas"]
        esps = d["esps"]
        if len(esps) > 0:
            # TODO: Include non-global deps for each esp
            esp_string = ',PLUGINS=[File("{}")]'.format(
                '"),File("'.join([esp["esp"] for esp in esps])
            )
        else:
            esp_string = ""

        INSTALL_DIRS.append(
            string.Template(
                """InstallDir("${directory}"${bsa_string}${esp_string})"""
            ).substitute(locals())
        )

    install_dir_string = ",".join(INSTALL_DIRS)

    build_file = string.Template(
        """# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1, InstallDir, File, Source$includes

class Mod(Pybuild1):
    NAME="${name}"
    DESC="${desc}"
    TAGS="${tags}"
    HOMEPAGE="${homepage}"
    LICENSE="TODO: FILLME"
    DEPENDS="${deps}"
    CONFLICTS="TODO: FILLME or Delete"
    KEYWORDS="TODO: FILLME or Delete"
    SOURCES=[Source("${url}")]
    INSTALL_DIRS=[${install_dir_string}]
    ${prepare}
    """
    ).substitute(locals())

    # User import repo may not exist. If not, create it
    if not os.path.exists(USER_REPO.location):
        os.makedirs(os.path.join(USER_REPO.location, "metadata"))
        metadata_file = os.path.join(USER_REPO.location, "metadata", "repo_name")
        with open(metadata_file, "w") as file:
            print("user-repo", file=file)
        # Add user repo to REPOS so that it can be used in further dependency resolution
        REPOS.append(USER_REPO)
        # Write user import repo to repos.cfg
        with open(REPOS_FILE, "a") as file:
            userstring = """
[user]
location = {}
auto_sync = False
masters = openmw
priority = 50
"""
            print(userstring.format(USER_REPO.location), file=file)

    outdir = os.path.join(USER_REPO.location, C, MN)
    filename = os.path.join(outdir, M + ".pybuild")
    os.makedirs(outdir, exist_ok=True)

    print("Exporting pybuild to {}".format(filename))
    with open(filename, "w") as file:
        print(build_file, file=file)

    # No python module for Black yet.
    black = shutil.which("black")
    if black:
        print("Formatting Pybuild...")
        subprocess.Popen(
            [black, filename], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        ).wait()
    else:
        print(
            '{}: could not find "black" to use for formatting pybuild'.format(
                colour(Fore.YELLOW, "WARNING")
            )
        )

    # Create manifest file
    shasum = get_hash(filename)
    size = os.path.getsize(filename)
    # Start by storing the pybuild in the manifest so that we can load it
    manifest = get_manifest(filename)
    manifest.add_entry(
        Manifest(os.path.basename(filename), FileType.PYBUILD, shasum, size)
    )
    manifest.write()

    create_manifest(load_file(filename))

    print(colour(Fore.GREEN, "Finished Importing {}".format(atom)))


def is_data_dir(directory):
    path = directory[0]
    for subdir in directory[1]:
        # Match textures and meshes directories
        if re.match("^([tT]extures|[mM]eshes|[vV]ideo|[sS]plash)$", subdir):
            return True
    for file in directory[2]:
        # Match .bsa, .esp and .esm files
        if re.match(
            "^.*\.([eE][sS][pP]|[bB][sS][aA]|[eE][sS][mM]|[oO][mM][wW][aA][dD][dD][oO][nN])$",
            file,
        ):
            return True
        if re.match("^.*\.([dD][dD][sS])$", file) and not re.match(
            ".*([Tt]extures|[iI]cons|[bB]ook[Aa]rt|[dD]istant[lL]and).*", path
        ):
            # Looks like its a textures directory that does not have a standard parent directory. We may not pick it up otherwise.
            print(
                "Detected textures in directory that is not a subdir of a common directory. This may be an optional set of textures, or it may be something else. Double-check before selecting this option: {}".format(
                    path
                )
            )
            return True
    return False


# Fixes structure of install directory and returns a list of location of any data directories found
def find_data_dirs(path):
    # Search for possible data directories and prompt the user to select the ones they want to include if multiple are found.
    return [os.path.relpath(x[0], path) for x in os.walk(path) if is_data_dir(x)]


# Finds bsas and esps in a mod installation directory
def find_esp_bsa(directory):
    esps = []
    bsas = []
    for file in os.listdir(os.path.normpath(directory)):
        if fnmatch.fnmatch(file, "*.[bB][sS][aA]"):
            bsas.append(file)
        elif (
            fnmatch.fnmatch(file, "*.[eE][sS][pPmM]")
            or fnmatch.fnmatch(file, "*.[oO][mM][wW][aA][dD][dD][oO][nN]")
            or fnmatch.fnmatch(file, "*.[oO][mM][wW][gG][aA][mM][eE]")
        ):
            esps.append(file)
    return (esps, bsas)
