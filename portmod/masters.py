# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import shutil
import subprocess
import re
import shlex

# Detects masters using tes3cmd
def get_masters(file):
    omwcmd = shutil.which("omwcmd")
    process = subprocess.Popen(
        shlex.split('{} -m "{}"'.format(omwcmd, file)),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    (output, err) = process.communicate()

    result = output.decode("utf-8", errors="ignore").rstrip("\n")
    if len(result) > 0:
        return set(result.split("\n"))
    return set()
