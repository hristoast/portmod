# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from colorama import init, Fore, Back, Style

init()


def green(text):
    return "{}{}{}".format(Fore.GREEN, text, Style.RESET_ALL)


def lgreen(text):
    return "{}{}{}".format(Fore.LIGHTGREEN_EX, text, Style.RESET_ALL)


def bright(text):
    return "{}{}{}".format(Style.BRIGHT, text, Style.RESET_ALL)


def colour(colour, text):
    return "{}{}{}".format(colour, text, Style.RESET_ALL)
