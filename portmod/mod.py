# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import sys
import shutil
import patoolib
import git
from colorama import Fore
from distutils.dir_util import copy_tree
from portmod.globals import MOD_DIR, TMP_DIR, INSTALLED_DB
from portmod.repo.download import download_mod
from portmod.repo.loader import load_installed_mod
from portmod.colour import colour


def remove_mod(mod):
    print("Removing " + colour(Fore.GREEN, mod.M))
    path = os.path.join(MOD_DIR, mod.C, mod.MN)

    mod.INSTALL_PATH = path
    mod.uninstall()
    mod.INSTALL_PATH = None

    if os.path.exists(path):
        shutil.rmtree(path)

    db_path = os.path.join(INSTALLED_DB, mod.C, mod.MN)
    if os.path.exists(db_path):
        # Remove and stage changes
        gitrepo = git.Repo.init(INSTALLED_DB)
        gitrepo.git.rm(os.path.join(mod.C, mod.MN), r=True, f=True)
        # Clean up unstaged files (e.g. pycache)
        shutil.rmtree(db_path)

    print("Finished Removing " + colour(Fore.GREEN, mod.M))


def install_mod(mod):
    print("Starting installation of " + colour(Fore.GREEN, mod.M))
    archives = download_mod(mod)
    if not archives and not mod.NO_DOWNLOAD:
        print("Unable to download mod. Aborting.")
        return False

    mod.SOURCE_DIR = os.path.join(TMP_DIR, mod.M, "src")
    mod.INSTALL_DIR = os.path.join(TMP_DIR, mod.M, "pkg")
    os.makedirs(mod.SOURCE_DIR, exist_ok=True)
    os.makedirs(mod.INSTALL_DIR, exist_ok=True)
    os.chdir(mod.SOURCE_DIR)

    print("Unpacking Mod...")
    for archive in archives:
        outdir = os.path.join(mod.SOURCE_DIR, os.path.basename(archive))
        os.makedirs(outdir)
        patoolib.extract_archive(archive, outdir=outdir)

    mod.prepare()

    final_install_dir = os.path.join(MOD_DIR, mod.C)
    os.makedirs(final_install_dir, exist_ok=True)
    final_install = os.path.join(final_install_dir, mod.MN)
    print("Installing into {}".format(final_install))
    if os.path.exists(final_install):
        remove_mod(mod)

    os.chdir(mod.INSTALL_DIR)
    mod.install()

    os.chdir(TMP_DIR)

    # If a previous version of this mod was already installed, remove it before doing the final copy
    old_mod = load_installed_mod(mod.CMN)
    if old_mod:
        remove_mod(old_mod)

    # Morrowind links the INSTALL_DIR to the morrowind install. Copy the link, not the morrowind install
    if os.path.islink(mod.INSTALL_DIR):
        linkto = os.readlink(mod.INSTALL_DIR)
        os.symlink(linkto, final_install)
    else:
        shutil.copytree(mod.INSTALL_DIR, final_install)

    # Update db entry for installed mod
    db_path = os.path.join(INSTALLED_DB, mod.C, mod.MN)
    gitrepo = git.Repo.init(INSTALLED_DB)
    os.makedirs(db_path, exist_ok=True)

    # Copy pybuild to DB
    shutil.copy(mod.FILE, db_path)
    gitrepo.git.add(os.path.join(mod.C, mod.MN, os.path.basename(mod.FILE)))

    # Copy Manifest to DB
    shutil.copy(os.path.join(os.path.dirname(mod.FILE), "Manifest"), db_path)
    gitrepo.git.add(os.path.join(mod.C, mod.MN, "Manifest"))

    # Copy installed use configuration to DB
    with open(os.path.join(db_path, "USE"), "w") as use:
        print(" ".join(mod.USE), file=use)
    gitrepo.git.add(os.path.join(mod.C, mod.MN, "USE"))

    # Copy repo pybuild was from to DB
    with open(os.path.join(db_path, "REPO"), "w") as repo:
        print(mod.REPO, file=repo)
    gitrepo.git.add(os.path.join(mod.C, mod.MN, "REPO"))

    print("Installed " + colour(Fore.GREEN, mod.M))

    return True
