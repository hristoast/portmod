# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import sys
from urllib.parse import urlparse
from pathlib import Path
from operator import itemgetter
from distutils.dir_util import copy_tree
from portmod.globals import MOD_DIR
from portmod.repo.atom import parse_atom
from portmod.config import read_config, write_config, add_config, remove_config
from portmod.repo.deps import satisfies_use
from portmod.repo.manifest import get_manifest
from portmod.repo.util import get_hash
from portmod.repo.metadata import (
    get_license,
    get_global_use,
    get_mod_metadata,
    license_exists,
)
from portmod.log import err
from portmod.colour import colour
from colorama import Fore
import inspect

# This is a terrible hack function, but it does the job. This should only be used within pybuild files
# macropy may provide a more robust way of implementing this
def ModInfo():
    frame = inspect.currentframe()
    try:
        g = frame.f_back.f_globals
        filename = g["__file__"]
        category = Path(filename).resolve().parent.parent.name
        ATOM = "{}/{}".format(category, os.path.basename(filename)[: -len(".pybuild")])
        (g["M"], g["MN"], g["MV"], g["MR"], g["CM"], g["D"]) = itemgetter(
            "M", "MN", "MV", "MR", "CM", "D"
        )(parse_atom(ATOM))

    finally:
        del frame


class InstallDir:
    def __init__(self, path, **kwargs):
        self.PATH = path
        self.USE = kwargs.get("USE", "").split()
        self.DESTPATH = kwargs.get("DESTPATH", ".")
        self.PLUGINS = kwargs.get("PLUGINS", [])
        self.ARCHIVES = kwargs.get("ARCHIVES", [])
        self.SOURCE = kwargs.get("SOURCE", None)
        self.RESOLVES = kwargs.get("RESOLVES", "").split()
        self.CONFLICTS = kwargs.get("CONFLICTS", "").split()


class File:
    def __init__(self, name, **kwargs):
        self.NAME = name
        self.USE = kwargs.get("USE", "").split()
        self.RESOLVES = kwargs.get("RESOLVES", "").split()
        self.CONFLICTS = kwargs.get("CONFLICTS", "").split()


class Source:
    def __init__(self, url, name=None):
        self.URL = url
        self.SHASUM = None
        self.SIZE = None
        self.NAME = name

    def manifest(self, shasum, size):
        self.SHASUM = shasum
        self.size = size


class Pybuild1:
    CONFLICTS = ""
    DEPENDS = ""
    DATA_OVERRIDES = ""
    IUSE = ""
    NO_DOWNLOAD = False
    TIER = "z"
    KEYWORDS = ""
    LICENSE = None
    NAME = None
    DESC = None
    HOMEPAGE = None
    REBUILD = ""

    def __init__(self):
        filename = sys.modules[self.__class__.__module__].__file__

        category = Path(filename).resolve().parent.parent.name
        self.ATOM = "{}/{}".format(
            category, os.path.basename(filename)[: -len(".pybuild")]
        )

        self.__REPO_PATH = Path(filename).resolve().parent.parent.parent
        repo_name_path = os.path.join(self.__REPO_PATH, "metadata", "repo_name")
        if os.path.exists(repo_name_path):
            with open(repo_name_path, "r") as repo_file:
                self.REPO = repo_file.readlines()[0].rstrip()
            self.DISPLAY_ATOM = "{}::{}".format(self.ATOM, self.REPO)

        (
            self.M,
            self.MN,
            self.MV,
            self.MR,
            self.C,
            self.R,
            self.CM,
            self.D,
            self.CMN,
        ) = itemgetter("M", "MN", "MV", "MR", "C", "R", "CM", "D", "CMN")(
            parse_atom(self.ATOM)
        )

        manifest = get_manifest(filename)

        if manifest is None:
            raise Exception("Manifest not found!")
        if manifest.get(os.path.basename(filename)) is None:
            raise Exception("Pybuild not in manifest file!")
        if manifest.get(os.path.basename(filename)).SHASUM != get_hash(filename):
            raise Exception("Pybuild does not match entry in manifest file!")

        for (index, source) in enumerate(self.SOURCES):
            if type(source) is Source:
                if source.NAME is None:
                    source.NAME = self.D
                name = source.NAME

            elif type(source) is str:
                name = os.path.basename(urlparse(source).path)
                self.SOURCES[index] = source = Source(source, name)
            else:
                raise TypeError(
                    'Source {} should be a "Source" or a str'.format(source)
                )

            if manifest is not None:
                if manifest.get(source.NAME) is not None:
                    m = manifest.get(source.NAME)
                    source.manifest(m.SHASUM, m.SIZE)

        for install in self.INSTALL_DIRS:
            if type(install) is type(InstallDir("")):
                if not self.NO_DOWNLOAD and install.SOURCE is None:
                    if any(source.NAME == self.D for source in self.SOURCES):
                        install.SOURCE = self.D
                    elif len(self.SOURCES) == 1:
                        install.SOURCE = self.SOURCES[0].NAME
                    else:
                        raise Exception(
                            "InstallDir does not declare a source name but source cannot be set automatically"
                        )
            else:
                raise TypeError(
                    "InstallDir {} should be of type InstallDir".format(install)
                )

        # Turn strings of space-separated atoms into lists
        if type(self.DEPENDS) is str:
            self.DEPENDS = self.DEPENDS.split()
        else:
            raise TypeError("DEPENDS must be a space-separated list of atoms")

        if type(self.CONFLICTS) is str:
            self.CONFLICTS = self.CONFLICTS.split()
        else:
            raise TypeError("CONFLICTS must be a space-separated list of atoms")

        if type(self.DATA_OVERRIDES) is str:
            self.DATA_OVERRIDES = self.DATA_OVERRIDES.split()
        else:
            raise TypeError("DATA_OVERRIDES must be a space-separated list of atoms")

        if type(self.IUSE) is str:
            self.IUSE = set(self.IUSE.split())
        else:
            raise TypeError("IUSE must be a space-separated list of use flags")

        if type(self.KEYWORDS) is str:
            self.KEYWORDS = set(self.KEYWORDS.split())
        else:
            raise TypeError("KEYWORDS must be a space-separated list of keywords")

        if type(self.TIER) is int:
            self.TIER = str(self.TIER)
        elif type(self.TIER) is not str:
            raise TypeError("TIER must be a integer or string containing 0-9 or z")

        if type(self.LICENSE) is str:
            self.LICENSE = set(self.LICENSE.split())
        else:
            raise TypeError(
                "LICENSE must be a string containing a space separated list of licenses"
            )

    def prepare(self):
        pass

    def update_config(self, config, install_dir):
        path = os.path.normpath(
            os.path.join(MOD_DIR, self.C, self.MN, install_dir.DESTPATH)
        )

        # Add data directory
        add_config(config, "data", '"' + path + '"')

        # Process BSAs
        for bsa in install_dir.ARCHIVES:
            if satisfies_use(bsa.USE, self):
                add_config(config, "fallback-archive", bsa.NAME)

        # Process ESPs
        for esp in install_dir.PLUGINS:
            if satisfies_use(esp.USE, self):
                add_config(config, "content", esp.NAME)

    def clean_config(self, config, install_dir):
        path = os.path.normpath(
            os.path.join(MOD_DIR, self.C, self.MN, install_dir.DESTPATH)
        )

        # Add data directory
        remove_config(config, "data", '"' + path + '"')

        # Process BSAs
        for bsa in install_dir.ARCHIVES:
            remove_config(config, "fallback-archive", bsa.NAME)

        # Process ESPs
        for esp in install_dir.PLUGINS:
            remove_config(config, "content", esp.NAME)

    def install(self):
        config = read_config()
        for install_dir in self.INSTALL_DIRS:
            if satisfies_use(install_dir.USE, self):
                print(
                    "Installing directory {} into {}".format(
                        install_dir.PATH, install_dir.DESTPATH
                    )
                )
                source = os.path.join(
                    self.SOURCE_DIR, install_dir.SOURCE, install_dir.PATH
                )
                dest = os.path.join(self.INSTALL_DIR, install_dir.DESTPATH)

                for file in install_dir.ARCHIVES + install_dir.PLUGINS:
                    # Remove files that aren't going to be used. We would like the user to enable them with use flags rather than manually
                    if not satisfies_use(file.USE, self):
                        os.remove(os.path.join(source, file.NAME))

                copy_tree(source, dest)
                self.update_config(config, install_dir)
            else:
                print(
                    "Skipping directory {} due to unsatisfied use requirements {}".format(
                        install_dir.PATH, install_dir.USE
                    )
                )
        write_config(config)

    def uninstall(self):
        config = read_config()
        for install_dir in self.INSTALL_DIRS:
            self.clean_config(config, install_dir)
        write_config(config)

    def validate(self):
        IUSE_STRIP = set([use.lstrip("+") for use in self.IUSE])
        errors = []
        for dep in self.DEPENDS + self.CONFLICTS + self.DATA_OVERRIDES:
            if parse_atom(dep) is None:
                errors.append('ERROR: atom "{}" is not valid'.format(dep))

        for install in self.INSTALL_DIRS:
            if type(install) != type(InstallDir("")):
                errors.append(
                    'InstallDir "{}" must have type InstallDir'.format(install.PATH)
                )
                continue
            for file in install.PLUGINS + install.ARCHIVES:
                if type(file) != type(File("")):
                    errors.append('File "{}" must have type File'.format(file))
                    continue

                for use in file.USE:
                    if use.lstrip("-+") not in IUSE_STRIP:
                        errors.append(
                            'Use flag "{}" for File "{}" must be declared in IUSE'.format(
                                use, file.NAME
                            )
                        )

                for atom in file.RESOLVES + file.CONFLICTS:
                    if parse_atom(atom) is None:
                        errors.append('ERROR: atom "{}" is not valid'.format(atom))

            if not self.NO_DOWNLOAD and not any(
                [install.SOURCE == source.NAME for source in self.SOURCES]
            ):
                errors.append(
                    'Source name "{}" was not declared in SOURCES'.format(
                        install.SOURCE
                    )
                )

            for use in install.USE:
                if use.lstrip("-+") not in IUSE_STRIP:
                    errors.append(
                        'Use flag "{}" for InstallDir "{}" must be declared in IUSE'.format(
                            use, install.PATH
                        )
                    )

            for atom in install.RESOLVES + install.CONFLICTS:
                if parse_atom(atom) is None:
                    errors.append('ERROR: atom "{}" is not valid'.format(atom))

        global_use = get_global_use(self.__REPO_PATH)
        metadata = get_mod_metadata(self)

        for use in IUSE_STRIP:
            if global_use.get(use) is None and (
                metadata is None or metadata.get("use", {}).get(use, None) is None
            ):
                errors.append(
                    'Use flag "{}" must be either a global use flag or declared in metadata.yaml'.format(
                        use
                    )
                )

        if self.NAME is None or "FILLME" in self.NAME or len(self.NAME) == 0:
            errors.append("Please fill in the NAME field")
        if self.DESC is None or "FILLME" in self.DESC or len(self.DESC) == 0:
            errors.append("Please fill in the DESC field")
        if (
            self.HOMEPAGE is None
            or "FILLME" in self.HOMEPAGE
            or len(self.HOMEPAGE) == 0
        ):
            errors.append("Please fill in the HOMEPAGE field")

        if self.LICENSE is None:
            errors.append(
                "You must specify a LICENSE for the mod (i.e., the License the mod uses)"
            )
        else:
            for license in self.LICENSE:
                if not license_exists(self.__REPO_PATH, license):
                    errors.append(
                        "LICENSE {} does not exit! Please make sure that it named correctly, or if it is a new License that it is added to the licenses directory of the repository".format(
                            license
                        )
                    )

        if len(errors) > 0:
            print(
                "Pybuild {} contains the following errors:".format(
                    colour(Fore.GREEN, self.FILE)
                )
            )
            for error in errors:
                err(error)
            return False

        return True

    def get_dir_path(self, install_dir):
        return os.path.join(MOD_DIR, self.C, self.MN, install_dir.DESTPATH)

    def get_file_path(self, install_dir, esp):
        return os.path.join(self.get_dir_path(install_dir), esp.NAME)
