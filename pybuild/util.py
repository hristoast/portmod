# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import shutil
import shlex
import subprocess


def tr_patcher(filename):
    process = subprocess.Popen(
        shlex.split('{} "{}"'.format(shutil.which("tr-patcher"), filename)),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
