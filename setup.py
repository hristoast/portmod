# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

#!/usr/bin/env python

from distutils.core import setup

setup(
    name="portmod",
    version="1.0",
    url="https://gitlab.com/portmod/portmod",
    scripts=["bin/mwmerge.py", "bin/mwuse.py", "openmw-conflicts"],
    packages=["portmod", "portmod.repo", "pybuild"],
)
